var Artists = require('../models/mariadb/artists');

/**
 * показать все записи
 * @param req
 * @param res
 */
exports.all = function (req, res) {
    Artists.all(req, function(err, rows){
        res(rows)
    });
};

/**
 * показать конкретную запись по id
 * @param req
 * @param res
 */
exports.findById = function (req, res) {
    Artists.findById(req, function (err, rows) {
        rows.forEach(function(value){
            if (value.val > 1) {
                console.log('>' + value.val);
            } else {
                console.log('<' + value.val);
            }
        });
        res(rows);
    });
};