var Auth = require('../models/mariadb/auth');

/**
 * Авторизация по логину и паролю
 * @param req
 * @param res
 */
exports.enter = function (req, res) {
    // err, возможные ошибки
    // rows полученные данные от модели
    Auth.enter(req, function(err, rows){
        if(rows !== null ){
            res({
                success: true,
                error: null,
                data: rows
            });
        } else {
            res({
                success: false,
                error: "login or password not correct",
                data: null
            });
        }
    });
};