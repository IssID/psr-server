// модуль сокетов для работы с сетью
var io = require('socket.io')({
    transports: ['websocket'] // транспорт через вебсокет
});
// порт подключения
io.attach(4567);

// логика работы по сети (router)
io.on('connection', function(socket){
    // console.log(socket.id); // id подключения по сокету

    // Авторизация.
    socket.on('Auth', function (data) {
        // подключаем контроллер авторизации
        var auth = require('./controllers/auth');

        // проверяем авторизацию
        auth.enter(data, function(res){
            io.emit('Auth', res);
        });
    });

    // тест на получение данных от сервера
    socket.on('getData', function (data) {
        // подключаем контроллер артистов
        var artistsController = require('./controllers/artists');

        // например при подключении нужно получить из базы данные о пользователе
        // передаем id пользователя
        if (data) {
            artistsController.findById(data, function(res){
                io.emit('sendData', res);
            });
        } else {
            artistsController.all('', function(res){
                io.emit('sendData', res);
            });
        }
    });

    // еслт получили сообщение об ошибке
    socket.on('error', function (data) {
        console.log(data);
    });

    // отправляем данные на сервер
    socket.on('sendServer', function (data) {
        console.log(data);
        // data.endPoint.x = "0";
        // data.endPoint.y = "-91.00001";
        data.Player = testPlayer;
        console.log(data);
        io.emit('sendAllCliens', data);

    });

    // отслеживаем событие отключения пользователя
    socket.on('disconnect', function (e) {
        console.log('disconnect');
        console.log(e);
    });
});

console.log("started");



























// var ws, users = []; // глобальные переменные

// io.on('connection', function (socket) {
// 	ws = socket; // this что бы не передавать в функции доп параметром
// 	var userUid = ws.id; // UID пользователя

// 	// приветствует подключившегося
// 	userConnect(userUid);

// 	// отслеживаем собитие sendServer от клиента
// 	socket.on('sendServer', function (data) {
// 		console.log(data);
// 		if(data.data === 'ping'){
// 			send('pong');
// 		}else{
// 			data.name = userUid; // указываем отправителя сообщения
// 			sendAll(data);
// 		}
// 	});

// 	socket.on('test', function (data) {
// 		console.log(data);
// 		ws.emit('test2', 'ypa');
// 	});

// 	ws.emit('test2', 'ypa');

// 	// отслеживаем событие отключения пользователя
// 	socket.on('disconnect', function () {
// 		userDisconnect();
// 	});
// });

// // пишем в консоль о запуске сервера
// console.log('chat-server started');

// // Добавляем в массив пользователей и приветствуем пользователя
// function userConnect(userUid)
// {
// 	// сообщаем что пользователь подключился
// 	sendAll({name: userUid, data: "connecned"});

// 	// добавляем в массив пользователя ws.id
// 	if(users.indexOf(userUid) === -1){
// 		users.push(userUid);
// 	}
// }

// // Удаляем пользователя из массива и сообщаем об этом других
// function userDisconnect()
// {
// 	// отправляем id отключившегося
// 	io.emit('disconnect', { del: users[users.indexOf(ws.id)]});

// 	// удаляем пользователя из списка
// 	users.splice(users.indexOf(ws.id), 1);
// }

// // Приватная отправка
// function send(data)
// {
// 	ws.emit('sendClien', data);
// }

// // Публичная отправка var data object
// function sendAll(data)
// {
// 	console.log('server 2: ' + data.data);
// 	io.emit('sendAllCliens', data);
// }

// process.on('message', function(m) {
// 	console.log('CHILD got message:', m);
// });
