// подключаем модуль базы данных mongo
var MongoClient = require('server/models/mongodb').MongoClient;

/**
 * объект модуля
 * @type {{db: null}}
 */
var state = {
    db: null
};

/**
 * подключение к базе
 * @param url
 * @param done
 * @returns {*}
 */
exports.connect = function (url, done) {
    if (state.db) {
        return done();
    }
    // реализуем подключение
    MongoClient.connect(url, {useUnifiedTopology: true, useNewUrlParser: true}, function (err, db) {
        if (err) {
            return done(err);
        }
        // подключение к базе (создается автоматически)
        state.db = db.db('students');
        done();
    })
};

/**
 * возращаем базу данных
 * @returns {null|*}
 */
exports.get = function () {
    return state.db;
};

