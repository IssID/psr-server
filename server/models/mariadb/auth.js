var db = require('../mariadb'); // подключение к базе данных

// @todo требуется приветсти к единому виду. через интерфейсы
// find, select []
// findOne, select {}
// save, inser {}
// update {}
// delete where id

// получаем все записи из базы
exports.enter = function (req, res) {
    db.get().query("SELECT * FROM users where login = ? and password = ?", [req.login, req.pass], function(err, rows){
        res(err, rows)
    });
};

// получаем конкретную запись по id из базы
exports.findById = function (req, res) {
    db.get().query("SELECT * FROM myTable where val = ?", req, function(err, rows){
        res(err, rows)
    });
};
