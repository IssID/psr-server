var db = require('../mariadb'); // подключение к базе данных

// получаем все записи из базы
exports.all = function (req, res) {
    db.get().query("SELECT * FROM myTable", function(err, rows){
        res(err, rows)
    });
};

// получаем конкретную запись по id из базы
exports.findById = function (req, res) {
    db.get().query("SELECT * FROM myTable where val = ?", req, function(err, rows){
        res(err, rows)
    });
};