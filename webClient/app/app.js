$(document).ready(function () {
    "use strict";
    let test;
    $('#canvas')[0].setAttribute("hidden", true);

    $('.enter').click(function (e) {
        let login = $('.login')[0];
        let pass = $('.pass')[0];

        if(login.value.length > 3 && pass.value.length > 3) {
            let obj = {login: login.value, pass: pass.value};
            socket.emit('Auth', obj);
        }
        socket.on('Auth', function(data){
            if (data.success) {
                $('#auth')[0].setAttribute("hidden", true);
                $('#canvas')[0].removeAttribute("hidden");
            }
        });
    });
});

function resetAuth() {
    $('#auth')[0].removeAttribute("hidden");
    $('#canvas')[0].setAttribute("hidden", true);
}