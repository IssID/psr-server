// -----------------------------------------------------------------------------------------------------------
var socket = io.connect('http://localhost:4567');

// Событи на подключение к серверу
socket.on('connect', function(){
    console.log("connect to server");
    // запрашиваем данные у сервера
    socket.emit('getData');
});

// получаем данные от сервера
socket.on('sendData', function(data){
    console.log(data);
});

// Событие на получение приватного сообщения от сервера
socket.on('sendClien', function(data){
    console.log('private: ' + data);

});

// Событие на получение приватного сообщения от сервера
socket.on('sendPathAll', function(data){
    renderPath(data.data);
});

// Событие на получение публичного сообщение (отправлено всем)
socket.on('sendAllCliens', function(data){
    console.log(data.name + ': ' + data.data );

});

// Событие на отключение от сервера
socket.on('disconnect', function(data){
    resetAuth(); // сбросить видимость канваса
    console.log(data.del);
});

// Отправить данные на сервер string or object (сервер сам должен обработать запрос)
function send(data)
{
    socket.emit('sendServer', { data: data});
}

// отправляем запрос на получение пути
function getdata(data)
{
    socket.emit('getData', data);
}